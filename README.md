# Scratch sudenkorento peli
Tämä projekti sopii ensimmäisiksi scratch ohjelmointi harjoituksiksi

## Valmiit koodit ja PDF ohje
Voit ladata tämän ohjeen PDF muodossa [täältä](./files/scratch-tutuksi.pdf)

Voit ladata kaikki koodin eri osiot valmina [täältä](./files/)

Valmis peli löytyy Scratch projektina [täältä](https://scratch.mit.edu/projects/909091058)

## Scratch koodaus tutuksi
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Scratch on lapsille, nuorille ja kaikille muillekin koodauksesta kiinnostuneille tarkoitettu ohjelmointikieli. Sit&auml; voi k&auml;ytt&auml;&auml; suoraan nettiselaimella tai sen voi ladata tietokoneelle.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Scratchilla voi tehd&auml; vaikka animaatiota tai koodata pelin. Hauskaa on my&ouml;s vain kokeilla kaikkia ihan p&auml;&auml;tt&ouml;mi&auml;kin juttuja, kokeilemalla oppii &#x1f642; Esim. legolla on my&ouml;s robottisarjoja, joita voi koodata scratchilla!</span></p>
<p class="c2"><span class="c1">&nbsp;</span></p>
<p class="c2"><span class="c1">Tutustaan t&auml;n&auml;&auml;n scratchiin selaimella.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Avataan osoite </span><span class="c24"><a class="c23" href="https://scratch.mit.edu/">https://scratch.mit.edu/</a></span><span>&nbsp;ja klikataan &ldquo;Create&rdquo; kohtaa niin p&auml;&auml;st&auml;&auml;n alkuun</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 601.70px; height: 528.00px;"><img alt="" src="./images/image61.png" style="width: 601.70px; height: 528.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<hr style="page-break-before:always;display:none;">
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Vaihdetaan kieleksi suomi.</span></p>
<p class="c2"><span class="c1">Klikataan rattaan kuvaa &ldquo;Settings&rdquo;</span></p>
<p class="c2"><span class="c1">Klikataan maapalln kuvaa &ldquo;Language&rdquo;</span></p>
<p class="c2"><span class="c1">Rullataan valintaa alasp&auml;in ja valitaan &ldquo;Suomi&rdquo;</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 601.70px; height: 528.00px;"><img alt="" src="./images/image16.png" style="width: 601.70px; height: 528.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Noniin nyt n&auml;ytt&auml;&auml; paremmalta!</span>
<hr style="page-break-before:always;display:none;">
</p>
<p class="c2"><span class="c1">Nyt alkuvalmistelut on tehty ja ruudun pit&auml;isi n&auml;ytt&auml;&auml; kutakuinkin t&auml;lt&auml;!</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Suljetaan viel&auml; vihre&auml; ruutu &ldquo;Sulje&rdquo; kohtaa klikkaamalla.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 601.70px; height: 528.00px;"><img alt="" src="./images/image71.png" style="width: 601.70px; height: 528.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<hr style="page-break-before:always;display:none;">
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Alla scratch ohjelman eri alueet</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 601.70px; height: 373.33px;"><img alt="" src="./images/image7.png" style="width: 601.70px; height: 373.33px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c3">Koodivalikosta </span><span class="c1">l&ouml;ytyy kaikki k&auml;yt&ouml;ss&auml; olevat toiminnot eli koodit.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c3">Koodiruudussa </span><span class="c1">on rakennettu ohjelmakoodi. Koodia rakennetaan valitsemalla koodivalikosta koodia ja raahaamalla sit&auml; koodiruutuun.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c3">Esiintymislavalla </span><span class="c1">n&auml;kyy mit&auml; koodi tekee. Koodi k&auml;ynnistet&auml;&auml;n klikkaamalla vihre&auml;t&auml; lippua ja pys&auml;ytet&auml;&auml;n painamalla sen vieress&auml; olevaa punaista stop merkki&auml;.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c3">Hahmot </span><span>ja </span><span class="c3">taustakuvat </span><span class="c1">valinnoista voidaan lis&auml;t&auml; ja poistaa esiintymislavalle hahmoja ja muuttaa taustakuvia.</span></p>
<p class="c0"><span class="c1"></span></p>
<hr style="page-break-before:always;display:none;">
<p class="c0"><span class="c1"></span></p>

## Osa 1 - Lentelevä sudenkorento
<p class="c2"><span class="c1">Tehd&auml;&auml;n sudenkorento, joka lent&auml;&auml; osoitettuun kohtaan ruudulla.</span></p>
<p class="c0"><span class="c1"></span></p>
<h3 class="c13" id="h.r2clpfs8zw7o"><span class="c6">Poistetaan kissahahmo</span></h3>
<p class="c2"><span class="c1">Aloitetaan poistamalla kissahahmo klikkaamalla roskiskuvaketta hahmon yl&auml;nurkassa.</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 87.00px; height: 84.00px;"><img alt="" src="./images/image50.png" style="width: 87.00px; height: 84.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<h3 class="c13" id="h.nv95j8u65sf1"><span class="c6">Lis&auml;t&auml;&auml;n sudenkorento</span></h3>
<p class="c2"><span>Lis&auml;t&auml;&auml;n uusi hahmo klikkaamalla </span><span class="c3 c9">valitse hahmo</span><span class="c1">&nbsp;kuvaketta</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 65.00px; height: 65.00px;"><img alt="" src="./images/image6.png" style="width: 65.00px; height: 65.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Hahmojen nimi&auml; ei ole suomennettu ja sudenkorento l&ouml;ytyy nimell&auml; &ldquo;dragonfly&rdquo;. Sit&auml; voi etsi&auml; selailemalla tai kirjoittamalla hakukentt&auml;&auml;n &ldquo;dragonfly&rdquo;. Kun se l&ouml;ytyy niin klikkaa sit&auml;.</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 601.70px; height: 437.33px;"><img alt="" src="./images/image15.png" style="width: 601.70px; height: 437.33px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<hr style="page-break-before:always;display:none;">
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Nyt ruudun pit&auml;isi n&auml;ytt&auml;&auml; t&auml;lt&auml;</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 421.00px; height: 355.00px;"><img alt="" src="./images/image51.png" style="width: 421.00px; height: 355.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Sudenkorento on hieman iso niin pienennet&auml;&auml;n sit&auml; hieman.</span></p>
<p class="c2"><span class="c1">Kirjoitetaan &ldquo;koko&rdquo; kentt&auml;&auml;n 30 ja painetaan enter. Nyt sudenkorento on sopivamman kokoinen t&auml;lle ruudulle.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 408.00px; height: 365.00px;"><img alt="" src="./images/image58.png" style="width: 408.00px; height: 365.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<hr style="page-break-before:always;display:none;">
<p class="c0"><span class="c1"></span></p>
<h3 class="c13" id="h.ncez55ezti5k"><span class="c6">Laitetaan sudenkorento lent&auml;m&auml;&auml;n, eli aletaan koodaamaan!</span></h3>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Etsit&auml;&auml;n koodivalikon </span><span class="c3">Tapahtumat</span><span>&nbsp;kohdasta </span><span class="c7 c4 c3">kun klikataan</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 113.00px; height: 54.00px;"><img alt="" src="./images/image18.png" style="width: 113.00px; height: 54.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c2"><span class="c1">ja raahataan se koodiruutuun.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 601.70px; height: 577.33px;"><img alt="" src="./images/image36.png" style="width: 601.70px; height: 577.33px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>! T&auml;m&auml; on aloitustapahtuma, joka suoritetaan aina kun </span><span class="c3 c11">vihre&auml;t&auml; </span><span class="c1">lippua painetaan.</span></p>
<hr style="page-break-before:always;display:none;">
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Lis&auml;t&auml;&auml;n tapahtumalle koodia.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Etsit&auml;&auml;n koodivalikon </span><span class="c3">Ohjaus</span><span>&nbsp;kohdasta </span><span class="c4 c3">ikuisesti</span><span class="c1">&nbsp;niminen lohko</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 116.00px; height: 79.00px;"><img alt="" src="./images/image45.png" style="width: 116.00px; height: 79.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c2"><span class="c1">ja raahataan se koodiruutuun.</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 601.70px; height: 580.00px;"><img alt="" src="./images/image65.png" style="width: 601.70px; height: 580.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">! T&auml;m&auml; on nimens&auml; mukaisesti ikuisesti py&ouml;riv&auml; ohjelmistosilmukka, eli kaikkea sen sis&auml;ll&auml; olevaa koodia py&ouml;ritet&auml;&auml;n jatkuvasti.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c3">Koodarit kutsuvat</span><span>&nbsp;t&auml;mm&ouml;ist&auml; koodia </span><span class="c3">ikiluupiksi </span><span>ja sit&auml; k&auml;ytet&auml;&auml;n monissa ohjelmissa!</span>
<hr style="page-break-before:always;display:none;">
</p>
<p class="c2"><span class="c1">Lis&auml;t&auml;&auml;n silmukan sis&auml;lle koodi joka tutkii onko hiiren nappia painettu.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Etsit&auml;&auml;n </span><span class="c3">Ohjaus</span><span>&nbsp;kohdasta </span><span class="c4 c3">jos</span><span class="c1">&nbsp;ohjauslohko</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 119.00px; height: 87.00px;"><img alt="" src="./images/image29.png" style="width: 119.00px; height: 87.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c2"><span>&nbsp;ja raahataan se </span><span class="c4 c3">ikuisesti</span><span class="c4">&nbsp;</span><span class="c1">lohkon sis&auml;lle.</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 601.70px; height: 578.67px;"><img alt="" src="./images/image9.png" style="width: 601.70px; height: 578.67px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c2"><span class="c1">T&auml;m&auml; lauseke tutkii onko jokin ehto totta ja jos on niin se suorittaa lohkon sis&auml;ll&auml; olevan koodin.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c3">Koodarit kutsuvat</span><span>&nbsp;t&auml;t&auml; </span><span class="c3">if-lauseeksi</span><span class="c1">&nbsp;(if on englanniksi jos). N&auml;it&auml; l&ouml;ytyy takuuvarmasti kaikista ohjelmista. T&auml;m&auml;n lis&auml;ksi on my&ouml;s if-else-lause (eli &ldquo;jos, tai muuten&rdquo; lause), jossa jos-haaran lis&auml;ksi my&ouml;s &ldquo;tai muuten&rdquo;-haara joka suoritetaan jos ehto ei ollutkaan totta.</span></p>
<hr style="page-break-before:always;display:none;">
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Seuraavaksi jos-lauseelle m&auml;&auml;ritell&auml;&auml;n tutkittava ehto.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Etsit&auml;&auml;n </span><span class="c3">Tuntoaisti</span><span>&nbsp;kohdasta ehto </span><span class="c7 c3 c8">onko hiiren nappi painettu?</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 163.00px; height: 34.00px;"><img alt="" src="./images/image33.png" style="width: 163.00px; height: 34.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c2"><span class="c1">ja raahataan se jos ehtolauseen ehtoruutuun.</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 601.70px; height: 578.67px;"><img alt="" src="./images/image52.png" style="width: 601.70px; height: 578.67px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c2"><span class="c1">Nyt meill&auml; on valmiina koodilohko joka suoritetaan aina kun hiiren nappia painetaan&hellip;</span></p>
<p class="c2"><span class="c1">&hellip;paitsi ett&auml; se ei tee mit&auml;&auml;n. Laitetaan seuraavaksi korentoon liikett&auml;!</span></p>
<hr style="page-break-before:always;display:none;">
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Etsit&auml;&auml;n </span><span class="c3">Liike</span><span>&nbsp;kohdasta </span><span class="c3 c12">liu&rsquo;u 1 sekuntia kohtaan x ja y</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 252.00px; height: 46.00px;"><img alt="" src="./images/image70.png" style="width: 252.00px; height: 46.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c2"><span class="c1">ja raahaa se jos lohko sis&auml;&auml;n (x ja y arvoista ei tarvi v&auml;litt&auml;&auml;, korjataan ne seuraavaksi).</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 601.70px; height: 578.67px;"><img alt="" src="./images/image49.png" style="width: 601.70px; height: 578.67px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c2"><span class="c1">T&auml;m&auml; koodi siirt&auml;&auml; hahmon haluttuun paikkaan </span></p>
<hr style="page-break-before:always;display:none;">
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Loppusilaus viel&auml; niin saadaan korento lentoon! Lis&auml;t&auml;&auml;n viel&auml; liukumiseen hiiren sijainnin x ja y arvot. Haetaan </span><span class="c3">Tuntoaisti</span><span>&nbsp;kohdasta </span><span class="c3 c8">hiiren x-sijainti</span><span>&nbsp;ja </span><span class="c3 c8">hiiren y-sijainti</span><span>&nbsp;ja raahataan ne </span><span class="c3 c12">liu&rsquo;uttajaan</span><span class="c1">.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>.</span><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 601.70px; height: 440.00px;"><img alt="" src="./images/image34.png" style="width: 601.70px; height: 440.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c0"><span class="c1"></span></p>
<hr style="page-break-before:always;display:none;">
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Jotta korennolla olisi kotoisampaa lis&auml;t&auml;&auml;n viel&auml; taustaksi mets&auml;.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Klikkaa oikeasta alalaidasta</span><span class="c9">&nbsp;</span><span class="c3 c9">Valitse tausta</span><span>&nbsp;</span><span class="c1">painiketta </span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 59.00px; height: 62.00px;"><img alt="" src="./images/image35.png" style="width: 59.00px; height: 62.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Tausta l&ouml;ytyy selailemalla tai sen voi hakea kirjoittamalla &ldquo;forest&rdquo; (mets&auml; englanniksi) hakukentt&auml;&auml;n. Kun oikea tausta l&ouml;ytyy niin klikkaa sit&auml;.</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 601.70px; height: 217.33px;"><img alt="" src="./images/image60.png" style="width: 601.70px; height: 217.33px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Nyt ruudun pit&auml;isi n&auml;ytt&auml;&auml; t&auml;lt&auml;, eka osa on valmis!</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 492.00px; height: 415.00px;"><img alt="" src="./images/image62.png" style="width: 492.00px; height: 415.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<hr style="page-break-before:always;display:none;">
<p class="c0"><span class="c1"></span></p>
<h3 class="c13" id="h.dq48lf56lije"><span class="c6">Nyt voit kokeilla lenn&auml;tt&auml;&auml; korentoa!</span></h3>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Klikkaa vihre&auml;t&auml; lippua k&auml;ynnist&auml;&auml;kseksi ohjelman. Kun ohjelma on k&auml;ynniss&auml; voit kokeilla klikata eri kohtia ruudulla ja korennon pit&auml;isi lent&auml;&auml; klikattuun kohtaan.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c0"><span class="c1"></span></p><a id="t.88fa8d151c5b76998762f31c3e763f2fcefc2348"></a><a id="t.0"></a>
<table class="c21">
    <tr class="c19">
        <td class="c26" colspan="1" rowspan="1">
            <p class="c2"><span class="c5 c3">! Lis&auml;tietoa innostuneille koodareille</span></p>
            <p class="c2"><span class="c1">(t&auml;m&auml;n voi ohittaa)</span></p>
            <p class="c2"><span>Hahmon sijainti ruudulla menee seuraavasti: kun x on isompi niin hahmo on oikealla ja jos x on pienempi hahmo on enemm&auml;n vasemmalla. Y toimii samoin korkeussuunnassa. Jos y on isompi niin hahmo on ylemp&auml;n&auml; ja jos y on pienempi niin hahmo on alempana. </span><span class="c3">Koodarit puhuvat x ja y koordinaateista.</span><span class="c1">&nbsp;Aivan keskell&auml; ruutua x on nolla ja y on nolla.</span></p>
        </td>
        <td class="c16" colspan="1" rowspan="1">
            <p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 248.13px; height: 161.64px;"><img alt="" src="./images/image41.png" style="width: 248.13px; height: 161.64px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
        </td>
    </tr>
</table>
<hr style="page-break-before:always;display:none;">
<hr style="page-break-before:always;display:none;">
<p class="c0"><span class="c1"></span></p>

## Osa 2 - Sudenkorennolle ruokaa!
<p class="c2"><span class="c1">Sudenkorennot sy&ouml;v&auml;t monipuolisesti erilaisia hy&ouml;nteisi&auml;, kuten k&auml;rp&auml;si&auml;, paarmoja, hyttysi&auml;, kirvoja ja perhosia, ja joskus jopa itse&auml;&auml;n pienempi&auml; sudenkorentoja. Hyttynen on viheli&auml;inen inisij&auml;, joten sy&ouml;tet&auml;&auml;n korennolle hyttysi&auml;. Pelist&auml; ei l&ouml;ydy hyttyst&auml; mutta pieni ruksi voi edustaa t&auml;ss&auml; tapauksessa hyttyst&auml;.</span></p>
<p class="c0"><span class="c1"></span></p>
<h3 class="c13" id="h.rus8uep9vi61"><span class="c6">Lis&auml;t&auml;&auml;n hyttynen</span></h3>
<p class="c2"><span>Lis&auml;t&auml;&auml;n uusi hahmo painamalla </span><span class="c3 c9">valitse hahmo</span><span class="c1">&nbsp;painiketta.</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 65.00px; height: 65.00px;"><img alt="" src="./images/image6.png" style="width: 65.00px; height: 65.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Etsit&auml;&auml;n ja lis&auml;t&auml;&auml;n hahmo </span><span class="c3">Button5</span><span class="c1">. Voit lis&auml;t&auml; sen samalla tavalla tavoin kuin korennonkin &#x1f642;</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 165.00px; height: 164.00px;"><img alt="" src="./images/image27.png" style="width: 165.00px; height: 164.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Ruksi on v&auml;h&auml;n iso hyttyseksi, joten muutetaan sen kooksi </span><span class="c3">20 </span><span>ja painetaan enter.</span><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 516.00px; height: 343.00px;"><img alt="" src="./images/image56.png" style="width: 516.00px; height: 343.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c2"><span class="c1">Nyth&auml;n se n&auml;ytt&auml;&auml; jo l&auml;hes hyttyselt&auml;&hellip; &hellip;ainakin v&auml;h&auml;n!</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Nyt kun lis&auml;ttiin toinen hahmo koodiruutu tyhjeni mutta ei h&auml;t&auml;&auml;. Hahmoa klikkaamalla saadaan valittua kyseisen hahmon koodit. Pidet&auml;&auml;n kuitenkin t&auml;ss&auml; kohtaa ruksi valittuna.</span>
<hr style="page-break-before:always;display:none;">
</p>
<h3 class="c13" id="h.xr7p13hj5ycf"><span class="c6">Koodataan hyttyseen liikett&auml;!</span></h3>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Laitetaan hyttynen &ldquo;syntym&auml;&auml;n&rdquo; johonkin paikkaan ruudulla ja katoamaan kun korento p&auml;&auml;ses tarpeeksi l&auml;helle</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Aloitetaan taas lis&auml;&auml;m&auml;ll&auml; </span><span class="c3 c22">vihre&auml;n </span><span>lipun klikkaus- eli aloitustapahtuma </span><span class="c4 c3 c7">kun klikataan</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 113.00px; height: 54.00px;"><img alt="" src="./images/image18.png" style="width: 113.00px; height: 54.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Lis&auml;t&auml;&auml;n tapahtuman alle </span><span class="c3">Liike</span><span>&nbsp;kohdasta </span><span class="c3 c12">mene sijaintiin</span><span>&nbsp;toiminto, jossa valittuna </span><span class="c5 c3">satunnainen sijainti</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 214.00px; height: 44.00px;"><img alt="" src="./images/image17.png" style="width: 214.00px; height: 44.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Lis&auml;t&auml;&auml;n sen alle taas </span><span class="c4 c3">ikuisesti</span><span class="c1">&nbsp;lohko (eli ikiluuppi)</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 116.00px; height: 79.00px;"><img alt="" src="./images/image45.png" style="width: 116.00px; height: 79.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Ja ikuisesti lohkon sis&auml;lle </span><span class="c4 c3">Jos , niin</span><span class="c1">&nbsp;lohko (lis&auml;t&auml;&auml;n t&auml;h&auml;n ehto seuraavalla sivulla)</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 119.00px; height: 87.00px;"><img alt="" src="./images/image29.png" style="width: 119.00px; height: 87.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Nyt hyttysen koodin pit&auml;isi n&auml;ytt&auml;&auml; t&auml;lt&auml;:</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 215.00px; height: 208.00px;"><img alt="" src="./images/image47.png" style="width: 215.00px; height: 208.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<hr style="page-break-before:always;display:none;">
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Lis&auml;t&auml;&auml;n seuraavaksi </span><span class="c3">Toiminnot</span><span>&nbsp;kohdasta </span><span class="c11 c3">pienempi kuin</span><span class="c1">&nbsp;vertailu, jolla tutkailemme et&auml;isyytt&auml; korentoon.</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 531.00px; height: 217.00px;"><img alt="" src="./images/image14.png" style="width: 531.00px; height: 217.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c2"><span class="c1">Laitetaan oikeanpuoleiseksi arvoon 25. T&auml;m&auml; on et&auml;isyys korentoon, jolloin hyttynen tulee sy&ouml;dyksi.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Lis&auml;t&auml;&auml;n vertailun vasemman puoleiseen arvoon </span><span class="c3">Tuntoaisti</span><span>&nbsp;kohdasta </span><span class="c3 c8">et&auml;isyys kohteeseen Dragonfly</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 601.70px; height: 213.33px;"><img alt="" src="./images/image69.png" style="width: 601.70px; height: 213.33px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c0"><span class="c5 c3"></span></p>
<p class="c0"><span class="c5 c3"></span></p>
<p class="c0"><span class="c3 c5"></span></p>
<p class="c2"><span class="c5 c3">Huom!</span></p>
<p class="c2"><span>K</span><span>ohteen voi valita klikkaamalla pikkuista nuolta alasp&auml;in </span><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 23.00px; height: 17.00px;"><img alt="" src="./images/image31.png" style="width: 23.00px; height: 17.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span><span class="c1">.</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 274.00px; height: 140.00px;"><img alt="" src="./images/image42.png" style="width: 274.00px; height: 140.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<hr style="page-break-before:always;display:none;">
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Lis&auml;t&auml;&auml;n seuraavaksi jos lauseen sis&auml;lle koodia, joka </span></p>
<p class="c2"><span class="c1">soittaa ja &auml;&auml;nen ja siirt&auml;&auml; hyttysen uuteen paikkaan.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Aloitetaan lis&auml;&auml;m&auml;ll&auml; </span><span class="c4 c3">jos, niin </span><span>lohkon sis&auml;lle </span><span class="c3">&Auml;&auml;ni</span><span>&nbsp;kohdasta </span><span class="c3 c9">soita &auml;&auml;ni pop</span><span class="c1">.</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 119.00px; height: 43.00px;"><img alt="" src="./images/image54.png" style="width: 119.00px; height: 43.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Lis&auml;t&auml;&auml;n jos lohkon sis&auml;lle viel&auml; </span><span class="c3">Liike</span><span>&nbsp;kohdasta </span><span class="c3 c12">mene sijaintiin satunnainen sijainti</span><span class="c1">.</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 214.00px; height: 44.00px;"><img alt="" src="./images/image17.png" style="width: 214.00px; height: 44.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Nyt koodin pit&auml;isi n&auml;ytt&auml;&auml; t&auml;lt&auml;:</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 372.00px; height: 279.00px;"><img alt="" src="./images/image38.png" style="width: 372.00px; height: 279.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Hyttysen koodi on nyt valmis ja voit kokeilla sit&auml;.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Paina </span><span class="c22 c3">vihre&auml;t&auml;</span><span class="c1">&nbsp;lippua k&auml;ynnist&auml;&auml;ksesi ohjelman.</span></p>
<p class="c2"><span class="c1">Etsi ruudulle ilmestynyt hyttynen ja klikkaa sen p&auml;&auml;ll&auml;.</span></p>
<p class="c2"><span class="c1">Sudenkorennon pit&auml;isi lent&auml;&auml; hyttysen luokse ja sy&ouml;d&auml; se (kuuluu &auml;&auml;ni ja hyttynen syntyy uuteen paikkaan).</span></p>
<p class="c0"><span class="c1"></span></p>
<hr style="page-break-before:always;display:none;">
<p class="c0"><span class="c1"></span></p>

## Osa 3 - Tehdään siitä peli!
<p class="c2"><span class="c1">Tehd&auml;&auml;n t&auml;st&auml; peli, jossa on minuutti aikaa sy&ouml;d&auml; niin monta hyttyst&auml; kuin ehtii!</span></p>
<p class="c0"><span class="c1"></span></p>
<h3 class="c13" id="h.1l4k29hd9ydc"><span class="c6">Luodaan pari muuttujaa: aika ja pisteet. </span></h3>
<p class="c2"><span class="c1">Muuttujat ovat numeroarvoja, jotka voivat muuttua pelin aikana.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Luodaan muuttuja ensiksi muuttuja aika.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Etsit&auml;&auml;n </span><span class="c3">Muuttujat</span><span>&nbsp;kohdasta painike </span><span class="c3">Tee muuttuja</span><span class="c1">&nbsp;ja klikataan sit&auml;.</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 308.00px; height: 413.00px;"><img alt="" src="./images/image28.png" style="width: 308.00px; height: 413.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c0"><span class="c1"></span></p>
<hr style="page-break-before:always;display:none;">
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Kirjoitetaan muuttujan nimi ja painetaan ok</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 396.00px; height: 334.00px;"><img alt="" src="./images/image53.png" style="width: 396.00px; height: 334.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Nyt aika muuttuja n&auml;kyy muuttujalistalla</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 134.00px; height: 121.00px;"><img alt="" src="./images/image1.png" style="width: 134.00px; height: 121.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Tehd&auml;&auml;n samalla tavalla viel&auml; toinen muuttuja, jonka nimi on pisteet</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 134.00px; height: 177.00px;"><img alt="" src="./images/image10.png" style="width: 134.00px; height: 177.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<hr style="page-break-before:always;display:none;">
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Seuraavaksi lis&auml;ill&auml;&auml;n vanha tuttu aloituslohko, t&auml;m&auml;n voi tehd&auml; kumman tahansa hahmon puolella mutta</span></p>
<p class="c2"><span class="c1">valitaan sudenkorento ellei se ole jo valittu</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 113.00px; height: 54.00px;"><img alt="" src="./images/image18.png" style="width: 113.00px; height: 54.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Ja t&auml;m&auml;n alle </span><span class="c3">Muuttujat</span><span>&nbsp;kohdasta </span><span class="c4 c3">aseta aika arvoon</span><span class="c1">&nbsp;toiminto. T&auml;m&auml; asettaa aloitusajaksi 60 sekuntia</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 170.00px; height: 48.00px;"><img alt="" src="./images/image57.png" style="width: 170.00px; height: 48.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c2"><span>Ja t&auml;m&auml; alle vastaavasti pisteiden asetus (muuttujan sai taas valittua klikkaamalla </span><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 16.00px; height: 14.00px;"><img alt="" src="./images/image63.png" style="width: 16.00px; height: 14.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span><span class="c1">)</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 184.00px; height: 46.00px;"><img alt="" src="./images/image68.png" style="width: 184.00px; height: 46.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Koodi n&auml;ytt&auml;&auml; nyt t&auml;lt&auml;</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 159.00px; height: 107.00px;"><img alt="" src="./images/image26.png" style="width: 159.00px; height: 107.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Lis&auml;t&auml;&auml;n seuraavaksi </span><span class="c3">Ohjaus</span><span>&nbsp;kohdasta </span><span class="c4 c3">toista kunnes</span><span class="c1">&nbsp;lohko</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 120.00px; height: 85.00px;"><img alt="" src="./images/image11.png" style="width: 120.00px; height: 85.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c2"><span class="c1">Lohko toistaa sis&auml;ll&auml; olevaa koodia niin kauan kun ehto on tosi</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c3">! Koodarit puhuu do-while </span><span class="c1">luupista (&ldquo;tee kunnes&rdquo; englanniksi &#x1f642;)</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Asetetaan toistoehto raahaamalla </span><span class="c3">Toiminnot</span><span>&nbsp;kohdasta </span><span class="c11 c3">on yht&auml;suuri kuin</span><span class="c3">&nbsp;</span><span class="c1">vertailu</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 107.00px; height: 36.00px;"><img alt="" src="./images/image30.png" style="width: 107.00px; height: 36.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c2"><span class="c1">Ja raahataan se toista kunnes lohkoon ja lis&auml;t&auml;&auml;n vertailun oikealle puolelle numero 0 ja vasemmalle puolelle &ldquo;Muuttujat&rdquo; kohdasta muuttuja aika.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Nyt koodi n&auml;ytt&auml;&auml; t&auml;lt&auml;</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 175.00px; height: 179.00px;"><img alt="" src="./images/image24.png" style="width: 175.00px; height: 179.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<hr style="page-break-before:always;display:none;">
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Lis&auml;t&auml;&auml;n lohkon sis&auml;lle koodia</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Etsit&auml;&auml;n </span><span class="c3">Ohjaus</span><span>&nbsp;kohdasta </span><span class="c4 c3">Odota 1 sekuntia</span><span>&nbsp;toiminto ja lis&auml;t&auml;&auml;n se </span><span class="c4 c3">toista kunnes</span><span class="c1">&nbsp;lohkon sis&auml;lle.</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 127.00px; height: 46.00px;"><img alt="" src="./images/image40.png" style="width: 127.00px; height: 46.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Lis&auml;t&auml;&auml;n viel&auml; </span><span class="c4 c3">toista kunnes</span><span>&nbsp;lohkon sis&auml;lle </span><span class="c3">Muuttujat</span><span>&nbsp;</span><span>kohdasta </span><span class="c4 c3">lis&auml;&auml; muuttujaan arvo</span><span class="c1">&nbsp;toiminto</span></p>
<p class="c2"><span class="c1">Huom arvo on -1 ja t&auml;m&auml; tarkoittaa sit&auml; ett&auml; ajasta v&auml;hennet&auml;&auml;n yksi</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 212.00px; height: 41.00px;"><img alt="" src="./images/image12.png" style="width: 212.00px; height: 41.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Lis&auml;t&auml;&auml;n ihan alimmaiseksi (&ldquo;toista kunnes&rdquo; lohkon alapuollelle) viel&auml; &ldquo;Ohjaus&rdquo; kohdasta &ldquo;pys&auml;yt&auml; kaikki&rdquo; toiminto.</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 118.00px; height: 43.00px;"><img alt="" src="./images/image25.png" style="width: 118.00px; height: 43.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Nyt koodin pit&auml;isi n&auml;ytt&auml;&auml; t&auml;lt&auml;</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 196.00px; height: 240.00px;"><img alt="" src="./images/image43.png" style="width: 196.00px; height: 240.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Jos ohjelman nyt k&auml;ynnist&auml;&auml; vihre&auml;st&auml; lipusta, pit&auml;isi ruudulla n&auml;ky&auml; aika, joka laskee kuudestakymmenest&auml; sekunnin v&auml;lein alasp&auml;in ja lopettaa kun aika on nollassa &#x1f642;</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Nyt puuttuu en&auml;&auml; toimiva pistelaskuri!</span></p>
<hr style="page-break-before:always;display:none;">
<p class="c0"><span class="c1"></span></p>
<h3 class="c13" id="h.1dm6yanhyse3"><span class="c6">Lis&auml;t&auml;&auml;n pistelaskuri</span></h3>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Valitaan hyttysen koodit klikkaamalla hahmovalinnasta hyttyst&auml; (...tai ruksia)</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 182.00px; height: 91.00px;"><img alt="" src="./images/image46.png" style="width: 182.00px; height: 91.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Ruudulla pit&auml;isi nyt n&auml;ky&auml; aiemmin luotu hyttysen koodi.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Lis&auml;t&auml;&auml;n </span><span class="c3">Muuttujat</span><span>&nbsp;kohdasta seuraavanlainen rivi </span><span class="c4 c3">Jos, niin</span><span class="c1">&nbsp;lohkon sis&auml;lle alimmaiseksi</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 216.00px; height: 44.00px;"><img alt="" src="./images/image5.png" style="width: 216.00px; height: 44.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Nyt koodin pit&auml;isi n&auml;ytt&auml;&auml; t&auml;lt&auml;</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 371.00px; height: 297.00px;"><img alt="" src="./images/image3.png" style="width: 371.00px; height: 297.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Nyt pelin pit&auml;isi toimia. K&auml;ynnist&auml; peli ja sy&ouml; niin monta hyttyst&auml; kuin ehdit!</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 283.50px; height: 232.03px;"><img alt="" src="./images/image55.png" style="width: 283.50px; height: 232.03px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<hr style="page-break-before:always;display:none;">
<p class="c0"><span class="c1"></span></p>

## Osa 4 - Pientä hienostelua
<p class="c0"><span class="c1"></span></p>
<h3 class="c13" id="h.knfk9t20lbg0"><span class="c6">Laitetaan sudenkorento pelin aluksi alas keskelle ruutua nokka yl&ouml;sp&auml;in.</span></h3>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Aloitetaan valitsemalla hahmovalikosta sudenkorento (&auml;l&auml; klikkaa vahingossa roskiskuvaketta ettei hahmo h&auml;vi&auml; &#x1f62f;). Nyt sudenkorennon koodit tulivat n&auml;kyviin.</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 186.00px; height: 115.00px;"><img alt="" src="./images/image67.png" style="width: 186.00px; height: 115.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Lis&auml;t&auml;&auml;n heti vihre&auml;n lipun klikkaustoiminnon alle </span><span class="c3">Liike</span><span>&nbsp;kohdasta </span><span class="c7 c3 c12">osoita suuntaan 90</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 129.00px; height: 45.00px;"><img alt="" src="./images/image59.png" style="width: 129.00px; height: 45.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span>Sen alle viel&auml; </span><span class="c3">Liike</span><span>&nbsp;kohdasta </span><span class="c3 c12">mene sijaintiin x , y</span><span class="c1">&nbsp;liike</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 185.00px; height: 44.00px;"><img alt="" src="./images/image32.png" style="width: 185.00px; height: 44.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Nyt aloituslohkon alla pit&auml;isi n&auml;ytt&auml;&auml;&auml; t&auml;lt&auml;</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 207.00px; height: 158.00px;"><img alt="" src="./images/image37.png" style="width: 207.00px; height: 158.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<hr style="page-break-before:always;display:none;">
<h3 class="c13 c17" id="h.7axg6kgho2k3"><span class="c6"></span></h3>
<h3 class="c13" id="h.gr22a4cpkldj"><span class="c6">Laitetaan sudenkorento lent&auml;m&auml;&auml;n nokka edell&auml; kohti ruokaa</span></h3>
<p class="c2"><span>Lis&auml;t&auml;&auml;n </span><span class="c4 c3">Jos , niin</span><span>&nbsp;lohkon sis&auml;lle alkuun </span><span class="c3">Liike</span><span>&nbsp;kohdasta </span><span class="c7 c3 c12">osoita kohti hiiren osoitin</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 168.00px; height: 49.00px;"><img alt="" src="./images/image64.png" style="width: 168.00px; height: 49.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c2"><span>ja heti sen alle </span><span class="c3">Liike</span><span>&nbsp;kohdasta </span><span class="c3 c12">K&auml;&auml;nny oikealle 90 astetta</span><span class="c1">&nbsp;toiminto</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 145.00px; height: 46.00px;"><img alt="" src="./images/image8.png" style="width: 145.00px; height: 46.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c2"><span class="c1">Huom. pieni ympyr&auml;nuoli pit&auml;&auml; osoittaa oikealle, muutoin korento lent&auml;&auml; peppu edell&auml; &#x1f642;</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Nyt ohjelmakoodin pit&auml;isi n&auml;ytt&auml;&auml; seuraaavalta:</span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 393.00px; height: 326.00px;"><img alt="" src="./images/image39.png" style="width: 393.00px; height: 326.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span class="c1">Nyt hienosteluosio on valmis. Kokeile toimiiko koodi oikein. Korennon pit&auml;isi nyt lent&auml;&auml; tyylikk&auml;&auml;sti p&auml;&auml; edell&auml; &#x1f642;</span></p>
<p class="c0"><span class="c1"></span></p>
<p class="c2"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 96.00px; height: 110.00px;"><img alt="" src="./images/image2.png" style="width: 96.00px; height: 110.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
<hr style="page-break-before:always;display:none;">